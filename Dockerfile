FROM ubuntu

WORKDIR /

RUN apt update 
RUN apt install -y curl gnupg

COPY install.sh /install.sh
RUN bash /install.sh
RUN rm /install.sh

COPY telegraf.conf /etc/telegraf/telegraf.conf

COPY supervisord /init
RUN chmod +x /init
RUN chmod 755 /init

CMD ["/init"]